import React from 'react'

import FormBuilder from "./pages/FormBuilder";

export default () => {
  return (
    <div>
      <FormBuilder />
    </div>
  );
}
