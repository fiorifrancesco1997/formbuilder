import React, {useCallback} from 'react'

import { Select } from 'antd'

import { includesInput } from '../../utils/appHelper'

export default (props) => {

  const {
    allowClear = true,
    filterOptions,
    ...rest
  } = props

  const customFilterOptions = useCallback((input, option) => filterOptions?.(input, option) || (
    includesInput(option?.label || option?.value, input)
  ), [filterOptions])

  return (
    <Select
      allowClear={allowClear}
      filterOption={customFilterOptions}
      {...rest}
    />
  )
}
