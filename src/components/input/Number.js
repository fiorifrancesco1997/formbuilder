import React from 'react'

import { InputNumber } from 'antd'

export default (props) => {
  const {
    allowClear = true,
    ...rest
  } = props
  return (
    <InputNumber
      allowClear={allowClear}
      {...rest}
    />
  )
}
