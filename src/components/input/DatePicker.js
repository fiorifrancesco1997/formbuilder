import React from 'react'

import { DatePicker } from 'antd'

export default (props) => {
  const {
    allowClear = true,
    ...rest
  } = props

  return (
    <DatePicker
      allowClear={allowClear}
      {...rest}
    />
  )
}
