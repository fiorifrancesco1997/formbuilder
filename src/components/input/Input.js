import React from 'react'

import { Input } from 'antd'

export default (props) => {
  const {
    allowClear = true,
    ...rest
  } = props
  return (
    <Input
      allowClear={allowClear}
      {...rest}
    />
  )
}
