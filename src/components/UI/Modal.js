import React from 'react'

import { Modal } from 'antd'

import styled from 'styled-components'

export default (props) => {
  const {
    width,
    height,
    fixedHeight = true,
    children,
    ...rest
  } = props
  return (
    <ModalStyle
      centered
      width={width}
      maxHeight={height}
      fixedHeight={fixedHeight}
      maskStyle={{ backgroundColor: 'rgba(0,0,0,.35)'}}
      { ...rest }
    >
      { children }
    </ModalStyle>
  )
}

const ModalStyle = styled(Modal)`
  &.ant-modal {
    max-width: calc(100vw - 32px);
    > .ant-modal-content {
      display: flex;
      flex-direction: column;
      ${(props) => props?.fixedHeight && `height: ${props?.fixedHeight}`};
      max-height: min(${(props) => props?.maxHeight || '100vh'}, calc(100vh - 32px));
      > .ant-modal-body {
        height: 100%;
        overflow-y: scroll;
        background-color: transparent;
      }
    }
  }
`
