import React from 'react'

import { Button } from 'antd'

export default ({ size = 'large', title, children, ...rest }) => {
  return (
    <Button
      size={size}
      {...rest}
    >
      { children || title }
    </Button>
  )
}
