import React from 'react'

import {Form, Row} from 'antd'

import { Button } from '../UI'
import FormItem from './FormItem'

export default (props) => {
  const {
    formRef,
    name,
    items = [],
    initialValues,
    onSubmit,
    onChange,
    submitTitle,
    resetTitle,
    children,
    ...rest
  } = props

  const [form] = Form.useForm()

  return (
    <Form
      name={name}
      form={form}
      initialValues={initialValues}
      onFinish={onSubmit}
      layout={'vertical'}
      onValuesChange={onChange}
      ref={formRef}
      {...rest}
    >
      { items.map((item) => (<FormItem {...item} key={item.key || item.name} />)) }
      { !(submitTitle || resetTitle) ? children : (
        <Row justify={'space-between'}>
          { resetTitle && (
            <Button
              type={'default'}
              onClick={() => form.resetFields()}
              title={resetTitle}
            />
          )}
          { submitTitle && (
            <Button
              type={'primary'}
              htmlType={'submit'}
              title={submitTitle}
            />
          )}
        </Row>
      )}

    </Form>
  )

}
