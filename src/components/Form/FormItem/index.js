import React from 'react'

import { Form } from 'antd'

import Input from "./Input";

export default (props) => {
  const {
    name,
    label,
    inputType,
    inputProps,
    CustomRender,
    ...rest
  } = props
  return (
    <Form.Item
      name={name}
      label={label}
      {...rest}
    >
      <Input
        type={inputType}
        inputProps={inputProps}
        CustomRender={CustomRender}
      />
    </Form.Item>
  )
}
