import React from 'react'

import * as I from '../../input'

export default ({ type, inputProps, CustomRender, ...rest }) => {

  const ControllerType = (type && I[type]) || CustomRender
  return (ControllerType && <ControllerType {...inputProps} {...rest} />) || (type ? 'missing input' : 'missing type')

}
