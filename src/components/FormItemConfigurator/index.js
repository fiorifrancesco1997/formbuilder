import React, {useCallback, useMemo, useState} from 'react'

import Form from "../Form";
import { Button, Modal } from '../UI'
import * as inputs from "../input";
import * as enums from '../../enumerations'
import {consolidateValues} from "./utils";

export default ({ onSubmit: onSubmitCallback }) => {

  const [open, setOpen] = useState(false)
  const [isSelect, setIsSelect] = useState(false)
  const formItems = useMemo(() => ([
    {
      name: 'name',
      label: 'Name',
      inputType: 'Input',
      rules: [{ required: true, message: 'Please input the item name' }],
    },
    {
      name: 'label',
      label: 'Title',
      inputType: 'Input',
      rules: [{ required: true, message: 'Please input the item title' }],
    },
    {
      name: 'inputType',
      label: 'Type',
      inputType: 'Select',
      rules: [{ required: true, message: 'Please select the item type' }],
      inputProps: {
        options: Object.keys(inputs).map((value) => ({ value, label: value }))
      },
    },
    ...(isSelect ? [
      {
        name: 'items',
        label: 'Items',
        inputType: 'Select',
        rules: [{ required: true, message: 'Please select the select items' }],
        inputProps: {
          options: Object.keys(enums).map((value) => ({ value, label: value }))
        }
      },
    ]: []),
  ]), [isSelect, setIsSelect])

  const onSubmit = useCallback((values) => {
    setOpen(false)
    onSubmitCallback?.(consolidateValues(values))
  }, [onSubmitCallback])

  return (
    <>
      <Button
        type={'default'}
        onClick={() => setOpen(true)}
      >
        Add form item
      </Button>
      <Modal
        open={open}
        onCancel={() => setOpen(false)}
        footer={(
          <Button
            title={'Submit'}
            htmlType={'submit'}
            form={'form-configurator'}
            type={'primary'}
          />
        )}
      >
        <Form
          name={'form-configurator'}
          onSubmit={onSubmit}
          onChange={(value) => value.inputType && setIsSelect(value.inputType === 'Select')}
          items={formItems}
        />
      </Modal>
    </>
  )
}
