import * as enums from '../../enumerations'

export const consolidateValues = (values) => {
  const { items, ...rest } = values
  return {
    ...(items && { inputProps: { options: enums[items] }}),
    ...rest
  }
}
