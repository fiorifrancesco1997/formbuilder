import _ from 'lodash'

export const compareLabels = (a, b) => {
  const stringA = a?.toString?.() || a
  const stringB = b?.toString?.() || b
  if (!_.isString(stringA) || !_.isString(stringB)) return false
  return stringA.toLowerCase().trim() === stringB.toLowerCase().trim()
}

export const includesInput = (label, input) => {
  const stringLabel = label?.toString?.() || label
  const stringValue = input?.toString?.() || input
  if (!_.isString(stringLabel) || !_.isString(stringValue)) return false
  return stringLabel.toLowerCase().includes(stringValue.toLowerCase().trim())
}
