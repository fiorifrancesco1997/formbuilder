export const enumerationA = [
  { key: '1', label: '1' },
  { key: '2', label: '2' },
  { key: '3', label: '3' },
]

export const enumerationB = [
  { key: 'A', label: 'A' },
  { key: 'B', label: 'B' },
  { key: 'C', label: 'C' },
]

export const enumerationC = [
  { key: 'aslkdn', label: 'asdk' },
  { key: 'sadkjnk', label: 'sadm akm' },
  { key: 'asdk', label: 'sdm asm' },
]
