import React, {useCallback, useRef, useState} from 'react'

import {Space, notification} from "antd";

import Form from '../components/Form'
import FormItemConfigurator from "../components/FormItemConfigurator";

export default () => {

  const formRef = useRef()
  const [formItems, setFormItems] = useState([])
  const onSubmit = useCallback((values) => {
    console.log(formItems)
    formRef?.current?.resetFields?.()
    notification.success({
      message: 'Success',
      description:
        'Your form has been saved correctly',
    });
  }, [formItems])

  return (
    <>
      <Space style={{ padding: '32px', width: '100%' }} size='large' direction={'vertical'}>
        <FormItemConfigurator
          onSubmit={(item) => setFormItems((prev) => [...prev, item])}
        />
        <Form
          name={'test-form'}
          submitTitle={!!formItems.length && 'Submit'}
          resetTitle={!!formItems.length && 'Reset'}
          formRef={formRef}
          items={formItems}
          onSubmit={onSubmit}
        />
      </Space>
    </>

  )
}
